---
title: "Krita 5.0.5 released"
date: "2022-04-14"
categories: 
  - "news"
  - "officialrelease"
---

Today we release Krita 5.0.5, probably the last release before Krita 5.1.0 comes out, hopefully in June. Note on the numbering: you didn't miss 5.0.3 and 5.0.4. Some stores, if they think a certain package isn't good enough, insist you upload a package with an increased version number, and that happened twice with 5.0.2...

Anyway, we worked hard and fixed a bunch of things!

- Fix artefacts when using the color smudge brush on CMYK images. [BUG:447211](https://bugs.kde.org/show_bug.cgi?id=447211)
- Fix blurry results in 4-point perspective transform. [BUG:447255](https://bugs.kde.org/show_bug.cgi?id=447255)
- Fix undo of liquify transform properties. [BUG:447314](https://bugs.kde.org/show_bug.cgi?id=447314)
- Fix the size of the About Krita dialog.
- Fix a crash when changing the Instant Preview settings of a brush preset.
- Fix use of layer names when using the G'Mic plugin. [BUG:447293](https://bugs.kde.org/show_bug.cgi?id=447293), [BUG:429851](https://bugs.kde.org/show_bug.cgi?id=429851)
- Add missing blending modes for G'Mic filters. [BUG:447293](https://bugs.kde.org/show_bug.cgi?id=447293)
- Fix issues with resource libraries with upper-case filename extensions. [BUG:447454](https://bugs.kde.org/show_bug.cgi?id=447454)
- Enable the OS file dialog on macOS by default.
- Fix broken macOS Arm64 canvas rendering for 16 bit/integer images.
- Fix crashes when undoing multiple layer operations too quickly. [BUG:447462](https://bugs.kde.org/show_bug.cgi?id=447462)
- Work around a crash in the transform mask applied to a passthrough group. [BUG:447506](https://bugs.kde.org/show_bug.cgi?id=447506)
- Fix the photoshop compatible shortcuts. [BUG:447771](https://bugs.kde.org/show_bug.cgi?id=447771)
- Fix AppimageUpdate. [BUG:446757](https://bugs.kde.org/show_bug.cgi?id=446757)
- Show the correct number of layers in the Image Properties dialog. Patch by Dan McCarthy, thanks!
- Fix the layout of the docker titlebars.
- Disable automatically assigning accelerator keys for selecting dockers.
- Fix a memory leak in the color history display.
- Fix a race condiction in the metadata system initialization.
- Fix animation playback when having multiple views on an animation. [BUG:450425](https://bugs.kde.org/buglist.cgi?quicksearch=450425)
- Fix dataloss when scaling an image with animated transform masks. [BUG:450781](https://bugs.kde.org/show_bug.cgi?id=450781)
- Fix incorrect basename for an animation when filepath or filename changes. [BUG:451654](https://bugs.kde.org/show_bug.cgi?id=451654)
- Fix restoring the default values for canvas input settings. [BUG:396064](https://bugs.kde.org/show_bug.cgi?id=396064)
- Update gradient colors correctly in layer styles. [BUG:452066](https://bugs.kde.org/show_bug.cgi?id=452066)
- Fix a crash when handling touch events.
- Fix a large number of defects found by Coverity.
- Fix the hitboxes in the curve widget being too small on HiDPI screens. [BUG:446755](https://bugs.kde.org/show_bug.cgi?id=446755)
- Fix saving over deactivated resources.
- Fix the Advanced Color Selector's hsySH square element. [BUG:452422](https://bugs.kde.org/show_bug.cgi?id=452422)
- Fix issues with cursors being too small on HiDPI screens. [BUG:448107](https://bugs.kde.org/show_bug.cgi?id=448107), [BUG:447314](https://bugs.kde.org/show_bug.cgi?id=447314)
- Fix problems with animating transparency and filter masks. [BUG:452170](https://bugs.kde.org/show_bug.cgi?id=452170)
- Opacity now loads correctly for animated vector layers. [BUG:452144](https://bugs.kde.org/show_bug.cgi?id=452144)
- Stop the storyboard docker creating frames on locked layers. [BUG:447396](https://bugs.kde.org/show_bug.cgi?id=447396)
- Storyboards: add the ability to duplicate existing scenes.
- Improve the interaction with ffmpeg when rendering animations or recordings.
- Improve detection of broken video sequences on import as animation.
- Work around a problem where not all pixels got copied when copying a mask. [BUG:453164](https://bugs.kde.org/show_bug.cgi?id=453164)
- Make it possible to save and load gradients on non-UTF8 locales. [BUG:447730](https://bugs.kde.org/show_bug.cgi?id=447730)
- Fix issue with masks and layers being broken after changing the colorspace.
- Update to LittleCMS 2.13.1 to fix a bug in the grayscale colorspace and fix conversion between 8 bits and 16 bits grayscale images. [BUG:447484](https://bugs.kde.org/show_bug.cgi?id=447484)
- Fix a delay on autobrush strokes with randomness: [BUG:446663](https://bugs.kde.org/show_bug.cgi?id=446663)
- Improve performance when moving a layer in a layer group. [BUG:450957](https://bugs.kde.org/show_bug.cgi?id=450957)
- Fix brush outline precision for pixel-art brushes. [BUG:447466](https://bugs.kde.org/show_bug.cgi?id=447466)
- Improve the brush outline for low-density brushes. [BUG:447045](https://bugs.kde.org/show_bug.cgi?id=447045)
- Set .apng as the default file extension for APNG files instead of PNG. [BUG:451473](https://bugs.kde.org/show_bug.cgi?id=451473)
- Fix crash when using the smart patch tool on HDR images. [BUG:451912](https://bugs.kde.org/show_bug.cgi?id=451912)
- Make it possible to set Weighted Distance to values larger than 100. [BUG:451874](https://bugs.kde.org/show_bug.cgi?id=451874)
- Fix the the fuzziness setting for the contiguous selection tool. [BUG:447524](https://bugs.kde.org/show_bug.cgi?id=447524)
- Fix the default eraser preset. [BUG:447650](https://bugs.kde.org/show_bug.cgi?id=447650)
- Add missing shortcuts for the following filters: slope/offset/power, cross-channel adjustment curves, halftone, gaussian high pass, height to normal map, gradient map, normalize and palettize. [BUG:451337](https://bugs.kde.org/show_bug.cgi?id=451337)
- Fix saving new workspaces. [BUG:446985](https://bugs.kde.org/show_bug.cgi?id=446985)
- Correctly handle the ICC profile blacklist.
- Fix the "Photoshop signature verification failed!"  warning if there are zero sized blocks in the PSD file. [BUG:450983](https://bugs.kde.org/show_bug.cgi?id=450983)
- Make starting Krita more robust if the configuration file is corrupted. [BUG:449983](https://bugs.kde.org/show_bug.cgi?id=449983)
- Add pressure and rotation to touch support.
- Fix translation issues on Android. [BUG:448343](https://bugs.kde.org/show_bug.cgi?id=448343)
- Fix autosaving on Android.
- Handle flags in SVG paths in vector layers correctly: [BUG:447417](https://bugs.kde.org/show_bug.cgi?id=447417)
- Fix crash when the selected gradient isn't shown in the gradient selector in the Layer Style dialog. Similar for patterns. [BUG:448296](https://bugs.kde.org/show_bug.cgi?id=448296), [BUG:445922](https://bugs.kde.org/show_bug.cgi?id=445922)
- Fix an issue in the brush preset selector if no brush preset has been selected. [BUG:449226](https://bugs.kde.org/show_bug.cgi?id=449226), [BUG:450121](https://bugs.kde.org/show_bug.cgi?id=450121)
- Fix a bug in Qt's accessibility handling. [BUG:449122](https://bugs.kde.org/show_bug.cgi?id=449122)
- Fix the menubar height in MDI mode with maximized subwindow on HiDPI screens. [BUG:449118](https://bugs.kde.org/show_bug.cgi?id=449118)
- Fix the speed sensor in the line tool. [BUG:434488](https://bugs.kde.org/show_bug.cgi?id=434488)
- Fix a crash in the raindrops filter when applied to an empty layer. [BUG:449408](https://bugs.kde.org/show_bug.cgi?id=449408)
- Fix a race condition in Select All. [BUG:449122](https://bugs.kde.org/show_bug.cgi?id=449122)
- Improve thread handling when painting, improving performance and energy efficiency. [BUG:367901](https://bugs.kde.org/show_bug.cgi?id=367901), [BUG:360677](https://bugs.kde.org/show_bug.cgi?id=360677)
- Improve the performance of the resource selectors if there are many resource libraries installed.
- Fix issues with saving tags when updating the database schema.
- Make it possible to tag and untag multiple resources at once.
- Reset the painting assistant after each stroke. [BUG:448187](https://bugs.kde.org/show_bug.cgi?id=448187)
- Fix issues witht he color smudge radius range. [BUG:441682](https://bugs.kde.org/show_bug.cgi?id=441682)
- Remove broken resource files after importing them fails. [BUG:446279](https://bugs.kde.org/show_bug.cgi?id=446279)
- Fix reference image update on resizing in HiDPI. [BUG:430988](https://bugs.kde.org/show_bug.cgi?id=430988)
- Fix issue with MyPaint presets when Slow Tracking interacts with the Stabilizer. [BUG:447562](https://bugs.kde.org/show_bug.cgi?id=447562)
- Fix banding when rendering in HDR mode. [BUG:445672](https://bugs.kde.org/show_bug.cgi?id=445672)
- Fix rendering SVG file layers. [BUG:448256](https://bugs.kde.org/show_bug.cgi?id=448256)
- Fix the small color selector on OpenGL 2.1. [BUG:447868](https://bugs.kde.org/show_bug.cgi?id=447868)
- Fix using the default theme if there is no theme configured. [BUG:448483](https://bugs.kde.org/show_bug.cgi?id=448483)
- Fix sizing of preview images in the non-native file dialog. [BUG:447805](https://bugs.kde.org/show_bug.cgi?id=447805)
- Improve handling of the CTRL modifier key. [BUG:438784](https://bugs.kde.org/show_bug.cgi?id=438784)
- Fix update of brush outline when switching brush preset or tool. [BUG:428988](https://bugs.kde.org/show_bug.cgi?id=428988), [BUG:442343](https://bugs.kde.org/show_bug.cgi?id=442343)
- Improve handling the thumbnails for the recent files list.
- Fix setting the configuration of a fill layer from the scripting API. [BUG:447807](https://bugs.kde.org/show_bug.cgi?id=447807)

{{< support-krita-callout >}}

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.0.5-setup.exe](https://download.kde.org/stable/krita/5.0.5/krita-x64-5.0.5-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.0.5.zip](https://download.kde.org/stable/krita/5.0.5/krita-x64-5.0.5.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/stable/krita/5.0.5/krita-x64-5.0.5-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.0.5-x86\_64.appimage](https://download.kde.org/stable/krita/5.0.5/krita-5.0.5-x86_64.appimage)

The separate gmic-qt appimage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.0.5.dmg](https://download.kde.org/stable/krita/5.0.5/krita-5.0.5.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface needs a large screen.

- [64 bits Intel CPU APK](https://download.kde.org/stable/krita/5.0.5/krita-x86_64-5.0.5-release-signed.apk)
- [32 bits Intel CPU APK](https://download.kde.org/stable/krita/5.0.5/krita-x86-5.0.5-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/stable/krita/5.0.5/krita-arm64-v8a-5.0.5-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/stable/krita/5.0.5/krita-armeabi-v7a-5.0.5-release-signed.apk)

### Source code

- [krita-5.0.5.tar.gz](https://download.kde.org/stable/krita/5.0.5/krita-5.0.5.tar.gz)
- [krita-5.0.5.tar.xz](https://download.kde.org/stable/krita/5.0.5/krita-5.0.5.tar.xz)

### md5sum

For all downloads:

- [md5sum.txt](https://download.kde.org/stable/krita/5.0.5/md5sum.txt)

### Key

The Linux appimage and the source .tar.gz and .tar.xz tarballs are signed. You can retrieve the public key [here](https://files.kde.org/krita/4DA79EDA231C852B). The signatures are [here](https://download.kde.org/stable/krita/5.0.5/) (filenames ending in .sig).
