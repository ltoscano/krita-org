---
title: "Krita 5.1.1 Released"
date: "2022-09-13"
categories: 
  - "news"
  - "officialrelease"
---

Today we're releasing Krita 5.1.1. This is strictly a bug fix release. Two serious problems are fixed in this release: a slowdown in start-up time experienced by some people, and a crash when copying a vector layer.

Other fixes are:

- Native macOS touchpad gestures should now work properly. [BUG:456446](https://bugs.kde.org/show_bug.cgi?id=456446)
- On Android, the application size no longer increases because swap files did not get deleted.
- On Android, a possible crash on startup is fixed. [BUG:458907](https://bugs.kde.org/show_bug.cgi?id=458907)
- Several issues with the MyPaint brush engine were fixed. The MyPaint eraser now uses the proper brush settings, and the unusable blend mode selector is now disabled if a MyPaint brush has been selected. [BUG:453054](https://bugs.kde.org/show_bug.cgi?id=453054), [BUG:445206](https://bugs.kde.org/show_bug.cgi?id=445206)
- For Animation, the "start numbering at" when exporting an image sequence has been fixed. [BUG:458997](https://bugs.kde.org/show_bug.cgi?id=458997)
- Krita no longer crashes if the user has removed the kritadefault.profile canvas input profile from both the installation and the runtime folder
- On reading ACO palettes, the color swatch name is now read and set on the swatch. [BUG:458209](https://bugs.kde.org/show_bug.cgi?id=458209)
- Fix a crash when selecting a layer in the layerbox. [BUG:458546](https://bugs.kde.org/show_bug.cgi?id=458546)
- Improve slider steps for the fade, ratio and similar color selection threshold
- When moving paintable nodes, the canvas is updated only once.
- Provide the workaround for OpenGL canvas showing black rectangles if there's more than one assistant visible to all platforms. [BUG:401940](https://bugs.kde.org/show_bug.cgi?id=401940)
- Improve the performance of working with ZIP files, like .kra and .ora files.
- Fix opening single-layer PSD files. [BUG:458556](https://bugs.kde.org/show_bug.cgi?id=458556)
- Make the Update link in the welcome widget clickable. [BUG:458034](https://bugs.kde.org/show_bug.cgi?id=458034)
- JPEG-XL: Fix linear HDR export and float 16 import. [BUG:458054](https://bugs.kde.org/show_bug.cgi?id=458054)

 

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.1.1-setup.exe](https://download.kde.org/stable/krita/5.1.1/krita-x64-5.1.1-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.1.1.zip](https://download.kde.org/stable/krita/5.1.1/krita-x64-5.1.1.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/stable/krita/5.1.1/krita-x64-5.1.1-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.1.1-x86\_64.appimage](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1-x86_64.appimage)

The separate gmic-qt appimage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.1.1.dmg](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface needs a large screen.

- [64 bits Intel CPU APK](https://download.kde.org/stable/krita/5.1.1/krita-x86_64-5.1.1-release-signed.apk)
- [32 bits Intel CPU APK](https://download.kde.org/stable/krita/5.1.1/krita-x86-5.1.1-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/stable/krita/5.1.1/krita-arm64-v8a-5.1.1-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/stable/krita/5.1.1/krita-armeabi-v7a-5.1.1-release-signed.apk)

### Source code

- [krita-5.1.1.tar.gz](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1.tar.gz)
- [krita-5.1.1.tar.xz](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1.tar.xz)

### md5sum

For all downloads, visit [https://download.kde.org/stable/krita/5.1.1/](https://download.kde.org/stable/krita/5.1.1) and click on Details to get the hashes.

### Key

The Linux appimage and the source .tar.gz and .tar.xz tarballs are signed. You can retrieve the public key [here](https://files.kde.org/krita/4DA79EDA231C852B). The signatures are [here](https://download.kde.org/stable/krita/5.1.1/) (filenames ending in .sig).
