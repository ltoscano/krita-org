---
title: "New Video! Making Brushes Part 4"
date: "2022-11-24"
categories: 
  - "news"
  - "tutorials"
---

Ramon has just published a new video, digging deeper than ever into brush preset creation in Krita! Enjoy!
 
{{< youtube FI5AUy9lcns >}}
