---
title: "Krita 5.1.0 公开测试第 2 版已经推出"
date: "2022-07-15"
categories: 
  - "news-zh"
  - "development-builds-zh"
---

今天我们为大家带来了 Krita 5.1 的公开测试第 2 版。您可以查看 [5.1 版的完整说明](https://krita.org/zh/krita-5-1-release-notes-zh/) (日后仍会补充)，也可以在下面观看 Wojtek Trybus 为本版软件制作的发布视频 (中国大陆用户需要科学上网)。

{{< youtube  TnvCjziCUGI >}}

 

![](images/2021-11-16_kiki-piggy-bank_krita5.png) Krita 是一个自由开源的软件项目，如果条件允许，请考虑通过[捐款](https://fund.krita.org)或[购买视频教程](https://krita.org/en/shop/)等方式为我们提供资金支持，这可以确保 Krita 的核心开发团队成员为项目全职工作。

## 已知问题

请注意：Krita 5.1 的公开测试第 1 版 (上一版) 存在一个程序缺陷，它会造成笔刷预设的保存异常。其他版本的 Krita 在加载受影响的笔刷预设时将会崩溃。如果您遇到了此问题，请使用此 [Python 脚本](https://invent.kde.org/tymond/brushes-metadata-fixer)来修复有问题的笔刷预设。

## 新增程序缺陷修复

Krita 5.1 公开测试第 2 版在[第 1 版](https://krita.org/zh/item/first-beta-for-krita-5-1-0-released-zh/)的基础上又修复了以下程序缺陷：

- 修复了在更换工作区时会连色板一起被更换的问题。[BUG:446634](https://bugs.kde.org/show_bug.cgi?id=446634)
- 修复了从位于非默认位置的资源文件夹加载模板。[BUG:452706](https://bugs.kde.org/show_bug.cgi?id=452706)
- 修复了在移除了一个标签后选中预设时偶尔会发生崩溃。[BUG:454052](https://bugs.kde.org/show_bug.cgi?id=454052)
- 保存工作区后清除输入框中的名称。[BUG:446652](https://bugs.kde.org/show_bug.cgi?id=446652)
- 修复了颜色涂抹笔刷引擎配置页面的循环更新崩溃。[BUG:455244](https://bugs.kde.org/show_bug.cgi?id=455244)
- 支持加载和保存名称带有半角句点的资源到资源包。[BUG:453702](https://bugs.kde.org/show_bug.cgi?id=453702)
- 支持多次使用 G'Mic 的曲线滤镜。[BUG:455891](https://bugs.kde.org/show_bug.cgi?id=455891)
- 修复了导出色板时的一处 assert 问题。[BUG:454949](https://bugs.kde.org/show_bug.cgi?id=454949)
- 修复了保存带有文字对象的图像为 PSD 时会导致崩溃。[BUG:455988](https://bugs.kde.org/show_bug.cgi?id=455988)
- 修复了图层色标选择器的无限循环递归问题。[BUG:456201](https://bugs.kde.org/show_bug.cgi?id=456201)
- 按照屏幕的 DPI 调整选区的蚂蚁线大小。[BUG:456364](https://bugs.kde.org/show_bug.cgi?id=456364)
- 降低了在图像当前未被显示又具有活动选区时的空闲 CPU 占用。[BUG:432284](https://bugs.kde.org/show_bug.cgi?id=432282)
- 修复了在某些情况下关闭设置对话框会导致崩溃。[BUG:445329](https://bugs.kde.org/show_bug.cgi?id=445329)
- 修复了在进行裁剪时网格设置失效的问题。[BUG:447588](https://bugs.kde.org/show_bug.cgi?id=447588)
- 修复了在撤销笔画时视图中的随机画面错误。[BUG:453243](https://bugs.kde.org/show_bug.cgi?id=453243)
- 最大图案比例提高到 10000%.
- 修复了当灰阶文件图层加载图像失败时的一处 assert 问题。[BUG:456201](https://bugs.kde.org/show_bug.cgi?id=456201)
- 修复在为 G'Mic 滤镜使用多个图层输入时的图层顺序。[BUG:456463](https://bugs.kde.org/show_bug.cgi?id=456463)
- 修复了当来源和目标图像的分辨率不同时拖放矢量图层。[BUG:456450](https://bugs.kde.org/show_bug.cgi?id=456450)
- 修复了在一次性移除多个图层时的崩溃。
- 修复了将矢量图层从一个图像拖放到另一个图像时的崩溃。[BUG:456450](https://bugs.kde.org/show_bug.cgi?id=456450)
- 修复了自动保存时图像保存功能中的一处会导致程序卡死的 assert 问题。[BUG:456404](https://bugs.kde.org/show_bug.cgi?id=456404)
- 闭合填充工具能够检测图像边界。[BUG:456299](https://bugs.kde.org/show_bug.cgi?id=456299)
- 修复了矩形的旋转。
- 修复了参考图像的拖放。[BUG:456382](https://bugs.kde.org/show_bug.cgi?id=4563822)
- 改善了笔刷轮廓光标类型的性能。[BUG:456080](https://bugs.kde.org/show_bug.cgi?id=456080)
- 修复了通过拖放添加的图层的名称。[BUG:455671](https://bugs.kde.org/show_bug.cgi?id=455671)
- 修复了粘贴图像的位置。[BUG:453247](https://bugs.kde.org/show_bug.cgi?id=453247)
- 修复了填充工具的快捷方式在填充工具使用了混合模式、橡皮擦模式或调整了不透明度时的功能。
- 修复了在对活动选区应用滤镜时的崩溃。[BUG:455844](https://bugs.kde.org/show_bug.cgi?id=455844)
- 改善了动画关键帧不透明度更改的撤销。[BUG:454547](https://bugs.kde.org/show_bug.cgi?id=454547)
- 修复了在复制的图层上设置不透明度关键帧时的崩溃。[BUG:454547](https://bugs.kde.org/show_bug.cgi?id=454547)
- 修复了在移动动画变形蒙版时的一处 assert 问题。
- 修复了预设笔尖图像的笔刷轮廓。[BUG:455912](https://bugs.kde.org/show_bug.cgi?id=455912)
- 在色标颜色更改后正确更新曲线网格渐变。
- 修复了矢量形状渐变的色标编辑。[BUG:455794](https://bugs.kde.org/show_bug.cgi?id=455794), [BUG:447464](https://bugs.kde.org/show_bug.cgi?id=447464), [BUG:449606](https://bugs.kde.org/show_bug.cgi?id=449606)
- 修复了导出动画为 JPEG-XL 格式时的问题。[BUG:455597](https://bugs.kde.org/show_bug.cgi?id=455597), [BUG:455598](https://bugs.kde.org/show_bug.cgi?id=455598)
- TIFF 图像支持 YCbCr 色彩模型和 JPEG 压缩方式。
- 支持加载分辨率为 1 PPI 的 Krita 图像。[BUG:444291](https://bugs.kde.org/show_bug.cgi?id=444291)
- 改善了导航器/总览图面板的刷新率。[BUG:443674](https://bugs.kde.org/show_bug.cgi?id=443674)
- 安卓版本：在状态栏使用正确的全屏图标。[BUG:456065](https://bugs.kde.org/show_bug.cgi?id=456065)
- Python 脚本：更改批注后将图像标记为已修改。[BUG:441704](https://bugs.kde.org/show_bug.cgi?id=441704)
- 修复了参考图像的复制和粘贴。[BUG:454515](https://bugs.kde.org/show_bug.cgi?id=454515)
- 修复了导入辅助尺组合后撤销时的崩溃。[BUG:455584](https://bugs.kde.org/show_bug.cgi?id=455584)
- 修复了使用 OpenColorIO 时的拖慢现象。
- 修复了灭点辅助尺和椭圆辅助尺的可见性。
- Python 脚本：如果无法获取指定的色彩特性文件，则使 Node::setColorSpace 返回 false。[BUG:454812](https://bugs.kde.org/show_bug.cgi?id=454812)
- 修复了分配“单指拖动”到缩放和旋转画布时的崩溃。[BUG:455241](https://bugs.kde.org/show_bug.cgi?id=455241)
- 修复了在图像正在处理中时打开 G'Mic 造成崩溃。
- 对所有 SVG 图标进行了优化。

## 下载

### 中文版信息

- 中文支持：Krita 的所有软件包均内建中文支持，首次安装时会自动设置为操作系统的语言。
- 手动设置：菜单栏 --> Settings --> Switch Application Language (倒数第二项) --> 下拉选单 --> 中文 (底部)，重启程序生效。
- 插件翻译：G'MIC 插件[需要下载翻译包](https://share.weiyun.com/SBopNjOn)
- 网盘下载：请在官网下载困难时使用，更新时间可能滞后。

### Windows 版本

- **64 位 Windows 安装程序** [本站下载](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-x64-5.1.0-beta2-setup.exe) | [网盘下载](https://share.weiyun.com/60HLzj6I)
- **64 位 Windows 免安装包** [本站下载](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-x64-5.1.0-beta2.zip) | [网盘下载](https://share.weiyun.com/60HLzj6I)
- **64 位程序崩溃调试包** [本站下载](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-x64-5.1.0-beta2-dbg.zip) | [网盘下载](https://share.weiyun.com/60HLzj6I)

- **配套网盘资源 (中文社区维护)** [中文离线文档](https://share.weiyun.com/Dea2uj0M) | [FFmpeg 软件包](https://share.weiyun.com/6tH13bVC) | [G'Mic 滤镜汉化](https://share.weiyun.com/SBopNjOn) |

- 32 位支持：最后一版支持 32 位 Windows 的 Krita 为 4.4.3，[本站下载](https://download.kde.org/stable/krita/4.4.3/krita-x86-4.4.3-setup.exe) | [网盘下载](https://share.weiyun.com/wdMnx1WB)。
- 免安装包：解压到任意位置，运行目录中的 Krita 快捷方式。不带文件管理器缩略图插件，与已安装版本共用配置文件和资源，但程序本身相互独立。
- 程序崩溃调试包：解压到 Krita 的安装目录，在报告程序崩溃问题时用于获取回溯追踪数据。日常使用无需下载此包。

### Linux 版本

- **64 位 Linux AppImage 程序包** [本站下载](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-5.1.0-beta2-x86_64.appimage) | [网盘下载](https://share.weiyun.com/C0gZ6joR)
- G'Mic 插件已经整合到主程序包中
- 如果浏览器把链接作为文本打开，请右键点击链接另存为文件。

### macOS 版本

- **macOS 程序映像** [本站下载](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-5.1.0-beta2.dmg) | [网盘下载](https://share.weiyun.com/gVg0CI53)

- 如果您还在使用 OSX Sierra 或者 High Sierra，请[观看此视频](https://www.youtube.com/watch?v=3py0kgq95Hk)了解如何启动由开发人员签名的可执行软件包。

### 安卓版本

Krita 现在已经能够在 ChromeOS 下正常运行，但其他安卓系统的支持目前尚处于测试阶段。安卓版的整体功能与桌面版本几乎完全相同，它尚未对手机等屏幕狭小细长的设备进行优化，也尚未开发平板专属的模式，因此建议搭配键盘使用。

- **64 位 Intel CPU APK 安装包** [本站下载](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-x86_64-5.1.0-beta2-release-signed.apk) | [网盘下载](https://share.weiyun.com/AKSomiNJ)
- **32 位 Intel CPU APK 安装包** [本站下载](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-x86-5.1.0-beta2-release-signed.apk) | [网盘下载](https://share.weiyun.com/AKSomiNJ)
- **64 位 ARM CPU APK 安装包** [本站下载](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-arm64-v8a-5.1.0-beta2-release-signed.apk) | [网盘下载](https://share.weiyun.com/GCKrtN0F)
- **32 位 ARM CPU APK 安装包** [本站下载](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-armeabi-v7a-5.1.0-beta2-release-signed.apk) | [网盘下载](https://share.weiyun.com/GCKrtN0F)

### 源代码

- [TAR.GZ 格式源代码包](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-5.1.0-beta2.tar.gz)
- [TAR.XZ 格式源代码包](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-5.1.0-beta2.tar.xz)
