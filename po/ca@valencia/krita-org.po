# Translation of krita-org.po to Catalan (Valencian)
# Copyright (C) 2022-2024 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# SPDX-FileCopyrightText: 2022, 2023, 2024 Antoni Bella Pérez <antonibella5@yahoo.com>
# Josep M. Ferrer <txemaq@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: websites-krita-org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-31 01:59+0000\n"
"PO-Revision-Date: 2024-06-16 15:30+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"SPDX-FileCopyrightText: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"SPDX-License-Identifier: LGPL-3.0-or-later\n"
"X-Generator: Lokalize 23.08.5\n"

#: config.yaml:0
msgid "Krita"
msgstr "Krita"

#: config.yaml:0
msgid ""
"Krita is a professional FREE and open source painting program. It is made by "
"artists that want to see affordable art tools for everyone."
msgstr ""
"Krita és un programa professional de pintura digital, lliure i de codi "
"obert. És realitzat per artistes que volen veure eines d'art assequibles per "
"a tothom."

#: config.yaml:0
msgid "Learn"
msgstr "Aprendre"

#: i18n/en.yaml:0
msgid "Recent Posts"
msgstr "Publicacions recents"

#: i18n/en.yaml:0
msgid "Donate"
msgstr "Donatius"

#: i18n/en.yaml:0
msgid "Software"
msgstr "Programari"

#: i18n/en.yaml:0
msgid "Education"
msgstr "Educació"

#: i18n/en.yaml:0
msgid "Foundation"
msgstr "Fundació"

#: i18n/en.yaml:0
msgid "Language"
msgstr "Idioma"

#: i18n/en.yaml:0
msgid "Report a bug"
msgstr "Informeu d'un error"

#: i18n/en.yaml:0
msgid "Roadmap"
msgstr "Full de ruta"

#: i18n/en.yaml:0
msgid "Release History"
msgstr "Historial de llançaments"

#: i18n/en.yaml:0
msgid "Documentation"
msgstr "Documentació"

#: i18n/en.yaml:0
msgid "Source Code"
msgstr "Codi font"

#: i18n/en.yaml:0
msgid "FAQ"
msgstr "PMF"

#: i18n/en.yaml:0
msgid "Privacy Statement"
msgstr "Declaració de privacitat"

#: i18n/en.yaml:0
msgid "Tutorials"
msgstr "Guies d'aprenentatge"

#: i18n/en.yaml:0
msgid "About"
msgstr "Quant a"

#: i18n/en.yaml:0
msgid "Donations"
msgstr "Donacions"

#: i18n/en.yaml:0
msgid "Get Involved"
msgstr "Col·laboreu-hi"

#: i18n/en.yaml:0
msgid "What is KDE"
msgstr "Què és KDE"

#: i18n/en.yaml:0
msgid "Website license"
msgstr "Llicència del lloc web"

#: i18n/en.yaml:0
msgid "Contact"
msgstr "Contacte"

#: i18n/en.yaml:0
msgid "Sitemap"
msgstr "Mapa del lloc"

#: i18n/en.yaml:0
msgid "Previous Post"
msgstr "Publicació anterior"

#: i18n/en.yaml:0
msgid "Next Post"
msgstr "Publicació següent"

#: i18n/en.yaml:0
msgid "{{ .ReadingTime }} minutes"
msgstr "{{ .ReadingTime }} minuts"

#: i18n/en.yaml:0
msgid "Reading time:"
msgstr "Temps de lectura:"

#: i18n/en.yaml:0
msgid "Could you tell us something about yourself? "
msgstr "Podries dir-nos alguna cosa sobre tu mateix? "

#: i18n/en.yaml:0
msgid "Download"
msgstr "Baixar"

#: i18n/en.yaml:0
msgid "Powered by"
msgstr "Funciona amb"

#: i18n/en.yaml:0
msgid "Art by "
msgstr "Art per "

#: i18n/en.yaml:0
msgid "News"
msgstr "Notícies"

#: i18n/en.yaml:0
msgid "No News"
msgstr "Cap notícia"

#: i18n/en.yaml:0
msgid "See All"
msgstr "Veure-ho tot"

#: i18n/en.yaml:0
msgid "Tools You Need To Grow as an Artist"
msgstr "Eines que necessitareu per a créixer com a artista"

#: i18n/en.yaml:0
msgid "All the features you need"
msgstr "Totes les característiques que necessiteu"

#: i18n/en.yaml:0
msgid ""
"Great for new and experienced artists. Here are some of the features that "
"Krita offers:"
msgstr ""
"Genial per a artistes nous i amb experiència. Estes són algunes de les "
"característiques que oferix Krita:"

#: i18n/en.yaml:0
msgid "Multiple brush types for different art styles"
msgstr "Múltiples tipus de pinzells per a estils d'art diferents"

#: i18n/en.yaml:0
msgid "Layers, drawing assistants, stabilizers"
msgstr "Capes, assistents de dibuix, estabilitzadors"

#: i18n/en.yaml:0
msgid "Animation tools to transform your artwork"
msgstr "Eines d'animació per a transformar el vostre treball artístic"

#: i18n/en.yaml:0
msgid "Resources and Materials"
msgstr "Recursos i materials"

#: i18n/en.yaml:0
msgid "Expand Krita’s capabilities with online tools and assets:"
msgstr "Expandiu les capacitats de Krita amb eines i recursos en línia:"

#: i18n/en.yaml:0
msgid "Brushes, patterns, and vector libraries"
msgstr "Pinzells, patrons i biblioteques vectorials"

#: i18n/en.yaml:0
msgid "Texture packs and page templates"
msgstr "Paquets de textures i plantilles de pàgines"

#: i18n/en.yaml:0
msgid "Plugins that add new functionality"
msgstr "Connectors que afigen característiques noves"

#: i18n/en.yaml:0
msgid "See Resources"
msgstr "Vegeu els recursos"

#: i18n/en.yaml:0
msgid "See features"
msgstr "Vegeu les característiques"

#: i18n/en.yaml:0
msgid "FREE education and resources"
msgstr "Educació i recursos lliures"

#: i18n/en.yaml:0
msgid "See education"
msgstr "Vegeu l'educació"

#: i18n/en.yaml:0
msgid "A supportive community"
msgstr "Una comunitat de suport"

#: i18n/en.yaml:0
msgid ""
"An online community for Krita artists to share artwork and tips with each "
"other."
msgstr ""
"Una comunitat en línia per als artistes de Krita per a compartir treballs "
"artístics i consells entre ells."

#: i18n/en.yaml:0
msgid "Share your artwork and get feedback to improve"
msgstr ""
"Compartiu el vostre treball artístic i obteniu comentaris per a millorar"

#: i18n/en.yaml:0
msgid "Ask questions about using Krita or seeing if there is a bug"
msgstr "Feu preguntes sobre com utilitzar Krita o veure si hi ha un error"

#: i18n/en.yaml:0
msgid ""
"Give feedback to developers when new features and plugins are being created"
msgstr ""
"Feu comentaris als desenvolupadors quan es creen característiques i "
"connectors nous"

#: i18n/en.yaml:0
msgid "Visit artist community"
msgstr "Visiteu la comunitat d'artistes"

#: i18n/en.yaml:0
msgid "Artist Interviews"
msgstr "Entrevistes amb artistes"

#: i18n/en.yaml:0
msgid "Free and Open Source"
msgstr "Codi font lliure i obert"

#: i18n/en.yaml:0
msgid ""
"Krita is a public project licensed as GNU GPL, owned by its contributors. "
"That's why Krita is Free and Open Source software, forever."
msgstr ""
"Krita és un projecte públic amb llicència GPL de GNU, propietat dels seus "
"col·laboradors. És per açò, que Krita és programari lliure i de codi obert, "
"per sempre."

#: i18n/en.yaml:0
msgid "See details about license usage"
msgstr "Vegeu els detalls sobre l'ús de la llicència"

#: i18n/en.yaml:0
msgid "Give back"
msgstr "Feu-nos costat"

#: i18n/en.yaml:0
msgid ""
"Krita is made by people from all around the world – many of which are "
"volunteers. If you find Krita valuable and you want to see it improve, "
"consider becoming part of the development fund."
msgstr ""
"Krita està fet per persones d'arreu del món -molts dels quals són "
"voluntaris-. Si trobeu valuós Krita i voleu veure com millora, considereu "
"formar part del patrocini de desenvolupament."

#: i18n/en.yaml:0
msgid "Contribute to the development fund"
msgstr "Contribuir al patrocini de desenvolupament"

#: i18n/en.yaml:0
msgid "Read more"
msgstr "Llegiu més"

#: i18n/en.yaml:0
msgid "Krita Sprint 2019 - Deventer, Netherlands"
msgstr "Krita Sprint 2019 - Deventer, Països Baixos"

#: i18n/en.yaml:0
msgid "No trials."
msgstr "Sense períodes de prova."

#: i18n/en.yaml:0
msgid "No subscriptions."
msgstr "Sense subscripcions."

#: i18n/en.yaml:0
msgid "No limit to your creativity."
msgstr "Sense límits a la vostra creativitat."

#: i18n/en.yaml:0
msgid "Clean and Flexible Interface"
msgstr "Una interfície neta i flexible"

#: i18n/en.yaml:0
msgid ""
"An intuitive user interface that stays out of your way. The dockers and "
"panels can be moved and customized for your specific workflow. Once you have "
"your setup, you can save it as your own workspace. You can also create your "
"own shortcuts for commonly used tools."
msgstr ""
"Una interfície d'usuari intuïtiva que no s'interposa en el vostre treball. "
"Els acobladors i les subfinestres es poden moure i personalitzar segons el "
"vostre flux de treball específic. Una vegada tingueu la configuració, podeu "
"guardar-la com a espai de treball. També podeu crear les vostres pròpies "
"dreceres per a eines d'ús habitual."

#: i18n/en.yaml:0
msgid "Customizable Layout"
msgstr "Disposició personalitzable"

#: i18n/en.yaml:0
msgid "Over 30 dockers for additional functionality"
msgstr "Més de 30 acobladors per a funcionalitats addicionals"

#: i18n/en.yaml:0
msgid "Dark and light color themes"
msgstr "Temes de color foscos i clars"

#: i18n/en.yaml:0
msgid "Learn the interface"
msgstr "Apreneu la interfície"

#: i18n/en.yaml:0
msgid "All the tools you need"
msgstr "Totes les eines que necessiteu"

#: i18n/en.yaml:0
msgid ""
"Over 100 professionally made brushes that come preloaded. These brushes give "
"a good range of effects so you can see the variety of brushes that Krita has "
"to offer."
msgstr ""
"Més de 100 pinzells fets professionalment que es precarreguen. Estos "
"pinzells donen un bon interval d'efectes perquè pugueu veure la varietat de "
"pinzells que oferix Krita."

#: i18n/en.yaml:0
msgid "Beautiful Brushes"
msgstr "Pinzells bonics"

#: i18n/en.yaml:0
msgid "Learn more"
msgstr "Aprendre'n més"

#: i18n/en.yaml:0
msgid "Brush Stabilizers"
msgstr "Estabilitzadors del pinzell"

#: i18n/en.yaml:0
msgid ""
"Have a shaky hand? Add a stabilizer to your brush to smoothen it out. Krita "
"includes 3 different ways to smooth and stabilize your brush strokes. There "
"is even a dedicated Dynamic Brush tool where you can add drag and mass."
msgstr ""
"Et tremola la mà? Afegiu un estabilitzador al pinzell per a suavitzar-lo. "
"Krita inclou 3 maneres diferents de suavitzar i estabilitzar les "
"pinzellades. Fins i tot hi ha una eina dedicada Pinzell dinàmic, des de la "
"qual podreu afegir arrossegament i massa."

#: i18n/en.yaml:0
msgid "Vector & Text"
msgstr "Vectors i text"

#: i18n/en.yaml:0
msgid ""
"Built-in vector tools help you create comic panels. Select a word bubble "
"template from the vector library and drag it on your canvas. Change the "
"anchor points to create your own shapes and libraries. Add text to your "
"artwork as well with the text tool. Krita uses SVG to manage its vector "
"format."
msgstr ""
"Les eines vectorials integrades vos ajuden a crear quadros de còmic. "
"Seleccioneu una plantilla de bambolles per a paraules des de la biblioteca "
"vectorial i arrossegueu-la fins al vostre llenç. Canvieu els punts "
"d'ancoratge per a crear les vostres pròpies formes i biblioteques. Afegiu "
"també text al vostre treball artístic amb l'eina de text. Krita utilitza SVG "
"per a gestionar el seu format vectorial."

#: i18n/en.yaml:0
msgid "Brush Engines"
msgstr "Motors de pinzell"

#: i18n/en.yaml:0
msgid ""
"Customize your brushes with over 9 unique brush engines. Each engine has a "
"large amount of settings to customize your brush. Each brush engine is made "
"to satisfy a specific need such as the Color Smudge engine, Shape engine, "
"Particle engine, and even a filter engine. Once you are done creating your "
"brushes, you can save them and organize them with Krita's unique tagging "
"system."
msgstr ""
"Personalitzeu els pinzells amb més de 9 motors únics de pinzell. Cada motor "
"disposa de moltes opcions de configuració per a personalitzar-los. Cada "
"motor de pinzell es fa per a satisfer una necessitat específica, com ara els "
"motors Esborronat del color, Formes, Partícules i, fins i tot un motor de "
"filtre. Una vegada hàgeu acabat de crear els pinzells, podeu guardar-los i "
"organitzar-los amb el sistema d'etiquetatge únic de Krita."

#: i18n/en.yaml:0
msgid "Wrap-around mode"
msgstr "Mode d'ajust cíclic"

#: i18n/en.yaml:0
msgid ""
"It is easy to create seamless textures and patterns now. The image will make "
"references of itself along the x and y axis. Continue painting and watch all "
"of the references update instantly. No more clunky offsetting to see how "
"your image repeats itself."
msgstr ""
"Ara és fàcil crear textures i patrons perfectes. La imatge crearà "
"referències de si mateixa al llarg de l'eix X i Y. Continueu pintant i "
"veureu totes les referències actualitzades a l'instant. Res de veure més "
"compensacions maldestres que es repetixen en la vostra imatge."

#: i18n/en.yaml:0
msgid "Resource Manager"
msgstr "Gestor de recursos"

#: i18n/en.yaml:0
msgid ""
"Import brush and texture packs from other artists to expand your tool set. "
"If you create some brushes that you love, share them with the world by "
"creating your own bundles. Check out the brush packs that are available in "
"the Resources area."
msgstr ""
"Importeu paquets de pinzells i textures d'altres artistes per a ampliar el "
"vostre conjunt d'eines. Si creeu alguns pinzells que vos agraden, compartiu-"
"los amb el món creant els vostres propis paquets. Consulteu els paquets de "
"pinzell disponibles en l'àrea de recursos."

#: i18n/en.yaml:0
msgid "Visit Resources Area"
msgstr "Visiteu l'àrea de recursos"

#: i18n/en.yaml:0
msgid "Simple and Powerful 2D Animation"
msgstr "Animació en 2D senzilla i potent"

#: i18n/en.yaml:0
msgid ""
"Turn Krita into an animation studio by switching to the animation workspace. "
"Bring your drawings to life by layering your animations, importing audio, "
"and fine tuning your frames. When you are finished, share with your friends "
"by exporting your creation to a video. Or just export the images to continue "
"working in another application."
msgstr ""
"Convertiu Krita en un estudi d'animació canviant a l'espai de treball "
"d'animació. Doneu vida als vostres dibuixos posant capes a les animacions, "
"important àudio i ajustant els fotogrames. Quan acabeu, compartiu amb els "
"amics exportant la vostra creació a un vídeo. O senzillament exporteu les "
"imatges per a continuar treballant en una altra aplicació."

#: i18n/en.yaml:0
msgid "Features"
msgstr "Característiques"

#: i18n/en.yaml:0
msgid "Multiple layers and audio support"
msgstr "Suport per a capes múltiples i àudio"

#: i18n/en.yaml:0
msgid "Supports 1,000s of frames on timeline"
msgstr "Permet 1.000 fotogrames en la línia de temps"

#: i18n/en.yaml:0
msgid "Playback controls with pausing, playing, and timeline scrubbing"
msgstr ""
"Controls de reproducció amb pausa, reproducció i rastreig de la línia de "
"temps"

#: i18n/en.yaml:0
msgid "Onion skinning support to help with in-betweens"
msgstr "Suport per a les pells de ceba per a ajudar amb els intermedis"

# https://ca.wikipedia.org/wiki/Tweening
#: i18n/en.yaml:0
msgid "Tweening with opacity and position changes"
msgstr ""
"Creació de fotogrames intermedis («tweening») amb canvis d'opacitat i posició"

#: i18n/en.yaml:0
msgid "Change start time, end time, and FPS"
msgstr "Canvia l'hora d'inici, l'hora final i els FPS"

# skip-rule: common-fixe
#: i18n/en.yaml:0
msgid "Export results to video or still images"
msgstr "Exporta els resultats cap a vídeo o imatges fixes"

#: i18n/en.yaml:0
msgid "Drag and drop frames to organize timings"
msgstr "Arrossegar i deixar anar fotogrames per a organitzar els temps"

#: i18n/en.yaml:0
msgid "Shortcuts for duplicating and pulling frames"
msgstr "Dreceres per a duplicar i estirar fotogrames"

#: i18n/en.yaml:0
msgid "Performance tweaking with drop-frame option"
msgstr "Reajustament del rendiment amb l'opció de descartar fotogrames"

#: i18n/en.yaml:0
msgid "Productivity features"
msgstr "Característiques de productivitat"

#: i18n/en.yaml:0
msgid "Drawing Assistants"
msgstr "Assistents de dibuix"

# skip-rule: kct-range
#: i18n/en.yaml:0
msgid ""
"Use a drawing aid to assist you with vanishing points and straight lines. "
"The Assistant Tool comes with unique assistants to help you make that "
"perfect shape. These tools range from drawing ellipses to creating "
"curvilinear perspective with the Fisheye Point tool."
msgstr ""
"Utilitzeu una assistent de dibuix per a ajudar-vos amb els punts de fuga i "
"les línies rectes. L'eina Assistent ve amb assistents únics per a ajudar-vos "
"a crear esta forma perfecta. Estes eines van des de dibuixar el·lipses fins "
"a crear una perspectiva curvilínia amb l'eina Punt d'ull de peix."

#: i18n/en.yaml:0
msgid "Layer Management"
msgstr "Gestió de capes"

#: i18n/en.yaml:0
msgid ""
"In addition to painting, Krita comes with vector, filter, group, and file "
"layers. Combine, order, and flatten layers to help your artwork stay "
"organized."
msgstr ""
"A més de pintar, Krita ve amb capes vectorials, de filtres, de grups i de "
"fitxers. Combinen, ordenen i aplanen les capes per a ajudar a mantindre el "
"treball artístic organitzat."

#: i18n/en.yaml:0
msgid "Select & Transform"
msgstr "Selecció i transformació"

#: i18n/en.yaml:0
msgid ""
"Highlight a portion of your drawing to work on. There are additional "
"features that allow you to add and remove from the selection. You can "
"further modify your selection by feathering and inverting it. Paint a "
"selection with the Global Selection Mask."
msgstr ""
"Ressalta una part del dibuix per a treballar-hi. Hi ha característiques "
"addicionals que permeten afegir i eliminar de la selecció. Es pot modificar "
"difuminant-la i invertint-la. Pinteu una selecció amb la Màscara de selecció "
"global."

#: i18n/en.yaml:0
msgid "Full Color Management"
msgstr "Gestió completa del color"

#: i18n/en.yaml:0
msgid ""
"Krita supports full color management through LCMS for ICC and OpenColor IO "
"for EXR. This allows you to incorporate Krita into your existing color "
"management pipeline. Krita comes with a wide variety of ICC working space "
"profiles for every need."
msgstr ""
"Krita permet la gestió completa del color a través de LCMS per a ICC i "
"l'OpenColor IO per a EXR. Permet incorporar Krita al flux existent de gestió "
"del color. Krita ve amb una àmplia varietat de perfils d'espai de treball "
"ICC per a cada necessitat."

#: i18n/en.yaml:0
msgid "GPU Enhanced"
msgstr "GPU millorada"

#: i18n/en.yaml:0
msgid ""
"With OpenGL or Direct3D enabled, you will see increased canvas rotation and "
"zooming speed. The canvas will also look better when zoomed out. The Windows "
"version supports Direct3D 11 in place of OpenGL."
msgstr ""
"Amb OpenGL o el Direct3D activats, veureu un augment de velocitat de gir del "
"llenç i del zoom. El llenç també es veurà millor quan es reduïsca. La versió "
"de Windows permet el Direct3D 11 en lloc d'OpenGL."

#: i18n/en.yaml:0
msgid "PSD Support"
msgstr "Suport per a PSD"

#: i18n/en.yaml:0
msgid ""
"Open PSD files that even Photoshop cannot open. Load and save to PSD when "
"you need to take your artwork across different programs."
msgstr ""
"Obri els fitxers PSD que fins i tot Photoshop no pot obrir. Carregueu i "
"guardeu a PSD quan necessiteu portar el vostre treball artístic a través de "
"programes diferents."

#: i18n/en.yaml:0
msgid "HDR Painting"
msgstr "Pintura en HDR"

#: i18n/en.yaml:0
msgid ""
"Krita is the only painting application that lets you open, save, edit and "
"author HDR and scene-referred images. With OCIO and OpenEXR support, you can "
"manipulate the view to examine HDR images."
msgstr ""
"Krita és l'única aplicació de pintura que permet obrir, guardar, editar i "
"crear imatges HDR i en referència a l'escena. Amb la implementació de l'OCIO "
"i l'OpenEXR, podeu manipular la vista per a examinar les imatges en HDR."

#: i18n/en.yaml:0
msgid "Python Scripting"
msgstr "Crear scripts en Python"

#: i18n/en.yaml:0
msgid ""
"Powerful API for creating your own widgets and extending Krita. With using "
"PyQt and Krita's own API, there are many possibilities. A number of plugins "
"come pre-installed for your reference."
msgstr ""
"Una API potent per a crear els vostres propis ginys i ampliar Krita. Amb "
"l'ús de PyQt i l'API pròpia de Krita, hi ha moltes possibilitats. Hi ha "
"diversos connectors preinstal·lats per a la vostra referència."

#: i18n/en.yaml:0
msgid "Training Resources"
msgstr "Recursos d'entrenament"

#: i18n/en.yaml:0
msgid ""
"In addition to training and educational material found on the Internet, "
"Krita produces its own training material to help you learn all of the tools "
"fast."
msgstr ""
"A més de la formació i el material educatiu que es troba a Internet, Krita "
"produïx el seu propi material d'entrenament per a ajudar-vos a aprendre "
"totes les eines ràpidament."

#: i18n/en.yaml:0
msgid "All of this (and more) for FREE"
msgstr "Tot açò (i més) debades"

#: i18n/en.yaml:0
msgid ""
"Krita is, and will always be, free software. There is a lot more to learn "
"than this overview page, but you should be getting a good idea of what Krita "
"can do."
msgstr ""
"Krita és, i sempre serà, programari lliure. Hi ha molt més a aprendre que "
"esta pàgina de resum, però hauríeu de tindre una bona idea del que pot fer "
"Krita."

#: i18n/en.yaml:0
msgid "Download Krita"
msgstr "Baixeu Krita"

#: i18n/en.yaml:0
msgid "Released on: "
msgstr "Publicat el: "

#: i18n/en.yaml:0
msgid "Release Notes"
msgstr "Notes de llançament"

#: i18n/en.yaml:0
msgid "Windows Installer"
msgstr "Instal·lador de Windows"

#: i18n/en.yaml:0
msgid "Windows Portable"
msgstr "Windows portable"

#: i18n/en.yaml:0
msgid "macOS Installer"
msgstr "Instal·lador de macOS"

#: i18n/en.yaml:0
msgid "Linux 64-bit AppImage"
msgstr "AppImage de Linux de 64 bits"

#: i18n/en.yaml:0
msgid "Flatpak"
msgstr "Flatpak"

#: i18n/en.yaml:0
msgid "snap"
msgstr "Snap"

#: i18n/en.yaml:0
msgid "Hosted on flathub."
msgstr "Allotjat en Flathub."

#: i18n/en.yaml:0
msgid "Maintained by the community."
msgstr "Mantingut per la comunitat."

#: i18n/en.yaml:0
msgid "Run"
msgstr "Executa"

#: i18n/en.yaml:0
msgid "Apple iPads and iPhones are not supported"
msgstr "Els iPad i els iPhone d'Apple no són compatibles"

#: i18n/en.yaml:0
msgid "Google Play Store"
msgstr "Botiga Google Play"

#: i18n/en.yaml:0
msgid "See individual Android APK builds"
msgstr "Vegeu les construccions APK individuals d'Android"

#: i18n/en.yaml:0
msgid "System Requirements"
msgstr "Requisits del sistema"

#: i18n/en.yaml:0
msgid " : "
msgstr " : "

#: i18n/en.yaml:0
msgid "Operating System"
msgstr "Sistema operatiu"

#: i18n/en.yaml:0
msgid "Windows 8.1 or higher / macOS 10.12 / Linux"
msgstr "Windows 8.1 o superior, macOS 10.12, Linux"

#: i18n/en.yaml:0
msgid "RAM"
msgstr "RAM"

#: i18n/en.yaml:0
msgid "4 GB at minimum required. 16 GB recommended."
msgstr "Es requerixen 4 GB com a mínim. Es recomana 16 GB."

#: i18n/en.yaml:0
msgid "GPU"
msgstr "GPU"

#: i18n/en.yaml:0
msgid "OpenGL 3.0 or higher / Direct3D 11"
msgstr "OpenGL 3.0 o superior, o Direct3D 11"

#: i18n/en.yaml:0
msgid "Graphics Tablet Brand"
msgstr "Marca de la tauleta gràfica"

#: i18n/en.yaml:0
msgid "Any tablet compatible with your operating system"
msgstr "Qualsevol tauleta compatible amb el sistema operatiu"

#: i18n/en.yaml:0
msgid "Store version"
msgstr "Versió de la botiga"

#: i18n/en.yaml:0
msgid ""
"Paid versions of Krita on other platforms. You will get automatic updates "
"when new versions of Krita come out. After deduction of the Store fee, the "
"money will support Krita development. For the Microsoft store version you "
"will need Windows 10 or above."
msgstr ""
"Versions de pagament de Krita en altres plataformes. Obtindreu "
"actualitzacions automàtiques quan isquen versions noves de Krita. Després de "
"la deducció de la tarifa de la botiga, els diners permetran el "
"desenvolupament de Krita. Per a la versió de la Microsoft Store necessitareu "
"Windows 10 o posterior."

#: i18n/en.yaml:0
msgid "Microsoft Store"
msgstr "Microsoft Store"

#: i18n/en.yaml:0
msgid "Steam Store"
msgstr "Steam Store"

#: i18n/en.yaml:0
msgid "Epic Games Store"
msgstr "Epic Games Store"

#: i18n/en.yaml:0
msgid "Mac App Store"
msgstr "Mac App Store"

#: i18n/en.yaml:0
msgid "Krita Next Nightly Builds"
msgstr "Krita Next, les construccions nocturnes"

#: i18n/en.yaml:0
msgid ""
"New builds created daily that have bleeding-edge features. This is for "
"testing purposes only. Use at your own risk."
msgstr ""
"Construccions noves creades diàriament que tenen característiques "
"d'avantguarda. Només amb finalitats de prova. Utilitzeu-les davall el vostre "
"propi risc."

#: i18n/en.yaml:0
msgid "Visit Krita Next"
msgstr "Visiteu Krita Next"

#: i18n/en.yaml:0
msgid "Krita Plus Nightly Builds"
msgstr "Krita Plus, les construccions nocturnes"

#: i18n/en.yaml:0
msgid ""
"Contains only bug fixes on top of the currently released version of Krita"
msgstr ""
"Només conté esmenes d'errors damunt de la versió de Krita actualment "
"publicada."

#: i18n/en.yaml:0
msgid "Visit Krita Plus"
msgstr "Visiteu Krita Plus"

#: i18n/en.yaml:0
msgid "Windows Shell Extension"
msgstr "Windows Shell Extension"

#: i18n/en.yaml:0
msgid ""
"The Shell extension is included with the Windows Installer. An optional add-"
"on for Windows that allow KRA thumbnails to appear in your file browser."
msgstr ""
"L'extensió Shell s'inclou amb l'instal·lador de Windows. Un complement "
"opcional per a Windows que permet que les miniatures KRA apareguen en el "
"navegador de fitxers."

#: i18n/en.yaml:0
msgid "Windows"
msgstr "Windows"

#: i18n/en.yaml:0
msgid "macOS"
msgstr "macOS"

#: i18n/en.yaml:0
msgid "Linux"
msgstr "Linux"

#: i18n/en.yaml:0
msgid ""
"Krita is a free and open source application. You are free to study, modify, "
"and distribute Krita under GNU GPL v3 license."
msgstr ""
"Krita és una aplicació lliure i de codi obert. Sou lliure d'estudiar, "
"modificar i distribuir Krita d'acord amb la llicència GNU GPL v3."

#: i18n/en.yaml:0
msgid "Download Older Versions"
msgstr "Baixada de versions antigues"

#: i18n/en.yaml:0
msgid ""
"If the newest version is giving you issues there are older versions "
"available for download. New versions of Krita on Windows do not support 32-"
"bit."
msgstr ""
"Si la versió més nova us està donant problemes, hi ha versions més antigues "
"disponibles per a descarregar. Les versions noves de Krita a Windows no "
"admeten 32 bits."

#: i18n/en.yaml:0
msgid "GPG Signatures"
msgstr "Signatures GPG"

#: i18n/en.yaml:0
msgid ""
"Used to verify the integrity of your downloads. If you don't know what GPG "
"is you can ignore it."
msgstr ""
"S'utilitza per a verificar la integritat de les baixades. Si no sabeu què és "
"GPG, podeu ignorar-ho."

#: i18n/en.yaml:0
msgid "Download Signature"
msgstr "Baixeu la signatura"

#: i18n/en.yaml:0
msgid "tarball"
msgstr "arxiu tar"

#: i18n/en.yaml:0
msgid "Git (KDE Invent)"
msgstr "Git (KDE Invent)"

#: i18n/en.yaml:0
msgid "Old Version Library"
msgstr "Biblioteca de versions antigues"

#: i18n/en.yaml:0
msgid "Last Windows 32-bit version"
msgstr "Última versió de Windows de 32 bits"

#: i18n/en.yaml:0
msgid "Hosted on snapcraft."
msgstr "Allotjat en Snapcraft."

#: i18n/en.yaml:0
msgid "Monthly Donations"
msgstr "Donacions mensuals"

#: i18n/en.yaml:0
msgid ""
"If you join the Krita Development Fund, you will directly help keep Krita "
"getting better and better, and you will get the following:"
msgstr ""
"Si vos uniu al Patrocini de desenvolupament de Krita, ajudareu directament a "
"mantindre Krita cada vegada millor, i obtindreu el següent:"

#: i18n/en.yaml:0
msgid ""
"A great application with powerful features, features designed together with "
"the Krita community"
msgstr ""
"Una aplicació genial amb característiques potents, dissenyades juntament amb "
"la comunitat Krita"

#: i18n/en.yaml:0
msgid ""
"Stable software where we will always try to fix issues—last year over 1,200 "
"issues were resolved!"
msgstr ""
"Programari estable on sempre intentarem solucionar problemes. L'any passat "
"es van resoldre més de 1.200 problemes!"

#: i18n/en.yaml:0
msgid "Visibility and recognition online, via community badges"
msgstr ""
"Visibilitat i reconeixement en línia, mitjançant distintius comunitaris"

#: i18n/en.yaml:0
msgid "Visit Development Fund"
msgstr "Visiteu el patrocini de desenvolupament"

#: i18n/en.yaml:0
msgid "One-time Donation"
msgstr "Donació única"

#: i18n/en.yaml:0
msgid ""
"One-time donations will take you to the Mollie payment portal. Donations are "
"done in euros as that provides the most payment options."
msgstr ""
"Una donació única vos portarà al portal de pagament Mollie. Les donacions es "
"fan en euros, ja que proporciona la majoria de les opcions de pagament."

#: i18n/en.yaml:0
msgid "Accepted payment methods:"
msgstr "Mètodes de pagament acceptats:"

#: i18n/en.yaml:0
msgid "Transaction ID: "
msgstr "ID de l'operació: "

#: i18n/en.yaml:0
msgid "Paid €%s"
msgstr "S'han pagat %s €"

#: i18n/en.yaml:0
msgid ""
"If there are issues with your donation, you can share this transaction ID "
"with us to help us know exactly which donation was yours."
msgstr ""
"Si hi ha problemes amb el vostre donatiu, podeu compartir este identificador "
"d'operació amb nosaltres i que ens ajudarà a saber amb exactitud quin "
"donatiu era el vostre."

#: i18n/en.yaml:0
msgid "Back to website"
msgstr "Torna al lloc web"

#: i18n/en.yaml:0
msgid "Have Fun Painting"
msgstr "Divertiu-vos pintant"

#: i18n/en.yaml:0
msgid "Having trouble downloading?"
msgstr "Teniu problemes amb les baixades?"

#: i18n/en.yaml:0
msgid "Visit the Downloads area"
msgstr "Visiteu l'àrea de baixades"

#: i18n/en.yaml:0
msgid "What's Next"
msgstr "Què toca ara"

#: i18n/en.yaml:0
msgid "Connect with the Community"
msgstr "Connecteu amb la comunitat"

#: i18n/en.yaml:0
msgid "Share artwork and ask questions on the forum."
msgstr "Compartiu el treball artístic i feu preguntes en el fòrum."

#: i18n/en.yaml:0
msgid "Krita Artists forum"
msgstr "Fòrum d'artistes de Krita"

#: i18n/en.yaml:0
msgid "Watch videos to learn about new brushes and techniques."
msgstr "Mireu els vídeos per a aprendre sobre pinzells i tècniques noves."

#: i18n/en.yaml:0
msgid "Krita YouTube channel"
msgstr "Canal de YouTube de Krita"

#: i18n/en.yaml:0
msgid "Free Learning Resources"
msgstr "Recursos d'aprenentatge lliures"

#: i18n/en.yaml:0
msgid "Getting Started"
msgstr "Com començar"

#: i18n/en.yaml:0
msgid "User Interface"
msgstr "Interfície d'usuari"

#: i18n/en.yaml:0
msgid "Basic Concepts"
msgstr "Conceptes bàsics"

#: i18n/en.yaml:0
msgid "Animations"
msgstr "Animacions"

#: i18n/en.yaml:0
msgid "User Manual"
msgstr "Manual d'usuari"

#: i18n/en.yaml:0
msgid "Layers and Masks"
msgstr "Capes i màscares"

#: i18n/en.yaml:0
msgid "Like what we are doing? Help support us"
msgstr "Vos agrada el que estem fent? Ajudeu-nos"

#: i18n/en.yaml:0
msgid ""
"Krita is a free and open source project. Please consider supporting the "
"project with donations or by buying training videos or the artbook! With "
"your support, we can keep the core team working on Krita full-time."
msgstr ""
"Krita és un projecte lliure i de codi obert. Considereu donar suport al "
"projecte amb donatius o comprant vídeos d'entrenament o el llibre artístic! "
"Amb el vostre suport, podrem mantindre l'equip principal treballant en Krita "
"a temps complet."

#: i18n/en.yaml:0
msgid "Buy something"
msgstr "Compreu alguna cosa"

#: i18n/en.yaml:0
msgid ""
"There should have been a video here but your browser does not seem to "
"support it."
msgstr ""
"Ací hauria d'haver-hi hagut un vídeo, però el vostre navegador no pareix que "
"el permeta."

#: i18n/en.yaml:0
msgid "Nope. Not in here either... (404)"
msgstr "No. Ací tampoc hi ha res… (404)"

#: i18n/en.yaml:0
msgid ""
"Either the page does not exist any more, or the web address was typed wrong."
msgstr "Ja no existix la pàgina o l'adreça web s'ha escrit malament."

#: i18n/en.yaml:0
msgid "Go back to the home page"
msgstr "Torna a la pàgina d'inici"
